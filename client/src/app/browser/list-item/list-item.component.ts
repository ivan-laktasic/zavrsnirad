import {Component, Input, OnInit} from '@angular/core';
import {FileModel} from '../../models/file.model';
import {MdDialog, MdDialogConfig} from "@angular/material";
import {MusicPropertiesDialogComponent} from "../music-properties-dialog/music-properties-dialog.component";
import {PicturePropertiesDialogComponent} from "../picture-properties-dialog/picture-properties-dialog.component";
import {VideoPropertiesDialogComponent} from "../video-properties-dialog/video-properties-dialog.component";
import {PictureFile} from "../../models/picture.file.model";
import {FileDataService} from "../../services/fileData.service";
import {MusicFile} from "../../models/music.file.model";
import {VideoFile} from "../../models/video.file.model";
import {PictureViewerDialogComponent} from "../picture-viewer-dialog/picture-viewer-dialog.component";
import {EmitterService} from "../../services/emitter.service";

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() listItem:FileModel


  type:Type;
  icon:string="folder";
  constructor(public dialog: MdDialog,public fileService:FileDataService,public emitterService:EmitterService) {}

  ngOnInit() {
    if(this.listItem.type['mime'].includes("image")){
      this.icon="photo"
      this.type=Type.Picture;
    }
    if(this.listItem.type['mime'].includes("audio")){
      this.icon="audiotrack"
      this.type=Type.Music;

    }
    if(this.listItem.type['mime'].includes("video")){
      this.icon="theaters"
      this.type=Type.Video;


    }
  }

  openProperties() {
    let config = new MdDialogConfig();
    if(this.type==Type.Picture){
      config.data=<PictureFile>this.listItem;
      this.dialog.open(PicturePropertiesDialogComponent,config);
    }
    if(this.type==Type.Music) {
      config.data = <MusicFile>this.listItem;
      this.dialog.open(MusicPropertiesDialogComponent,config);
    }
    if(this.type==Type.Video) {
      config.data = <VideoFile>this.listItem;
      this.dialog.open(VideoPropertiesDialogComponent,config);
    }
  }

  deleteItem(){
    if(this.type==Type.Picture){
      this.fileService.deletePictureFile(this.listItem.location).subscribe((response)=>{
        this.fileService.filesUpdated.emit(true);
      });
    }
    if(this.type==Type.Music) {
      this.fileService.deleteMusicFile(this.listItem.location).subscribe((response)=>{
        this.fileService.filesUpdated.emit(true);
      });
    }
    if(this.type==Type.Video) {
      this.fileService.deleteVideoFile(this.listItem.location).subscribe((response)=>{
        this.fileService.filesUpdated.emit(true);
      });
    }
  }

  openItem(){
    let config = new MdDialogConfig();
    if(this.type==Type.Picture){
      config.data=<PictureFile>this.listItem;
      this.dialog.open(PictureViewerDialogComponent,config);
    }
    if(this.type==Type.Music) {
      this.emitterService.musicFileToPlay.emit(<MusicFile> this.listItem);
    }
    if(this.type==Type.Video) {
      this.emitterService.videoFileToPlay.emit(<VideoFile> this.listItem);

    }
  }
}

enum Type{
  Picture,
  Video,
  Music
}
