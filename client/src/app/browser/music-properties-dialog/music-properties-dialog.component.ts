import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";
import {FileDataService} from "../../services/fileData.service";
import {MusicFile} from "../../models/music.file.model";
import {Response} from "@angular/http";

@Component({
  selector: 'app-music-properties-dialog',
  templateUrl: './music-properties-dialog.component.html',
  styleUrls: ['./music-properties-dialog.component.css']
})
export class MusicPropertiesDialogComponent implements OnInit {

  edit:boolean=false;
  inputFilename:string;
  artist:string;
  album:string;
  genre:string;


  constructor(public dialogRef: MdDialogRef<MusicPropertiesDialogComponent>,
              @Optional() @Inject(MD_DIALOG_DATA) private music: MusicFile,
              private fileService:FileDataService) {}

  ngOnInit() {
    this.inputFilename=this.music.filename.substr(0, this.music.filename.lastIndexOf('.')) || this.music.filename;
    this.fileService.getMusicData(this.music.location).subscribe(
      (response:Response)=>{
        console.log(response);
        if(response.status==206){
          this.fileService.registerMusicFile(this.music).subscribe((postResponse)=>{
            this.music=postResponse.json();
            this.artist=this.music.artist;
            this.album=this.music.album;
            this.genre=this.music.genre;
          });
        }else{
          this.music=response.json();
        }
      }

    )
  }

  editToogle(){
    this.edit=!this.edit;
  }

  close(){
    this.dialogRef.close();
  }

  update(){
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(this.music.filename)[1];
    this.music.album=this.album;
    this.music.artist=this.artist;
    this.music.genre=this.genre;
    this.music.filename=this.inputFilename+'.'+ext;
    this.fileService.updateMusicFile(this.music).subscribe((response)=>{
      if(response.status==200){
        this.fileService.filesUpdated.emit(true);
        this.dialogRef.close();
      }else{
        console.log(response.statusText);
      }
    })
  }

}
