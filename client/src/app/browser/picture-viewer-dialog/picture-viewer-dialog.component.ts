import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from "@angular/material";
import {PictureFile} from "../../models/picture.file.model";
import {FileDataService} from "../../services/fileData.service";


@Component({
  selector: 'app-picture-viewer-dialog',
  templateUrl: './picture-viewer-dialog.component.html',
  styleUrls: ['./picture-viewer-dialog.component.css']
})
export class PictureViewerDialogComponent implements OnInit {

  pictureLocation:string;

  constructor(public dialogRef: MdDialogRef<PictureViewerDialogComponent>,
              @Optional() @Inject(MD_DIALOG_DATA) private picture: PictureFile,
              private fileService:FileDataService) { }

  ngOnInit() {
    this.pictureLocation=this.fileService.pictureDataUrl+'/file/'+this.picture.location;
  }

  close(){
    this.dialogRef.close();
  }

}
