import {Component, Inject, Input, OnInit, Optional} from '@angular/core';
import {MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from "@angular/material";
import {PictureFile} from "../../models/picture.file.model";
import {FileDataService} from "../../services/fileData.service";
import {Response} from "@angular/http";

@Component({
  selector: 'app-picture-properties-dialog',
  templateUrl: './picture-properties-dialog.component.html',
  styleUrls: ['./picture-properties-dialog.component.css']
})
export class PicturePropertiesDialogComponent implements OnInit {

  edit:boolean=false;
  inputFilename:string;
  description:string;

  constructor(public dialogRef: MdDialogRef<PicturePropertiesDialogComponent>,
              @Optional() @Inject(MD_DIALOG_DATA) private picture: PictureFile,
              private fileService:FileDataService) {}


  ngOnInit() {
    this.inputFilename=this.picture.filename.substr(0, this.picture.filename.lastIndexOf('.')) || this.picture.filename;
    this.fileService.getPictureData(this.picture.location).subscribe(
      (response:Response)=>{
        console.log(response);
        if(response.status==206){
          this.fileService.registerPictureFile(this.picture).subscribe((postResponse)=>{
            this.picture=postResponse.json();
            this.description=this.picture.description;
          });
        }else{
          this.picture=response.json();
        }
      }

    )
  }

  editToogle(){
    this.edit=!this.edit;
  }

  close(){
    this.dialogRef.close();
  }

  update(){
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(this.picture.filename)[1];
    this.picture.description=this.description
    this.picture.filename=this.inputFilename+'.'+ext;
    this.fileService.updatePictureFile(this.picture).subscribe((response)=>{
      if(response.status==200){
        this.fileService.filesUpdated.emit(true);
        this.dialogRef.close();
      }else{
        console.log(response.statusText);
      }
    })
  }

}


