import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";
import {VideoFile} from "../../models/video.file.model";
import {FileDataService} from "../../services/fileData.service";
import{Response} from "@angular/http";

@Component({
  selector: 'app-video-properties-dialog',
  templateUrl: './video-properties-dialog.component.html',
  styleUrls: ['./video-properties-dialog.component.css']
})
export class VideoPropertiesDialogComponent implements OnInit {

  edit:boolean=false;
  inputFilename:string;
  director:string;
  actors:string;
  genre:string;

  constructor(public dialogRef: MdDialogRef<VideoPropertiesDialogComponent>,
              @Optional() @Inject(MD_DIALOG_DATA) private video: VideoFile,
              private fileService:FileDataService) {}

  ngOnInit() {
    this.inputFilename=this.video.filename.substr(0, this.video.filename.lastIndexOf('.')) || this.video.filename;
    this.fileService.getVideoData(this.video.location).subscribe(
      (response:Response)=>{
        console.log(response);
        if(response.status==206){
          this.fileService.registerVideoFile(this.video).subscribe((postResponse)=>{
            this.video=postResponse.json();
            this.director=this.video.director;
            this.actors=this.video.actors;
            this.genre=this.video.genre;
          });
        }else{
          this.video=response.json();
        }
      }

    )
  }

  editToogle(){
    this.edit=!this.edit;
  }

  close(){
    this.dialogRef.close();
  }

  update(){
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(this.video.filename)[1];
    this.video.director=this.director;
    this.video.actors=this.actors;
    this.video.genre=this.genre;
    this.video.filename=this.inputFilename+'.'+ext;
    this.fileService.updateVideoFile(this.video).subscribe((response)=>{
      if(response.status==200){
        this.fileService.filesUpdated.emit(true);
        this.dialogRef.close();
      }else{
        console.log(response.statusText);
      }
    })
  }

}
