import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {BrowserService} from "../services/browser.service";
import {Response} from "@angular/http";
import {FileModel} from "../models/file.model";
import {FileDataService} from "../services/fileData.service";
;

@Component({
  selector: 'app-browser',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.css']
})
export class BrowserComponent implements OnInit {

  @Input('initPath') initPath:string;

  readonly rootFolder:string='Server/mediaFolder';
  filesList: Array<FileModel>;
  topFolder: Array<string>;

  constructor(private browserService:BrowserService,private fileService:FileDataService) {
    this.fileService.filesUpdated.subscribe((status:boolean)=>{
      if(status){
        this.ngOnInit();
        this.fileService.filesUpdated.emit(false);
      }
    })
  }

  ngOnInit() {
    this.topFolder=[this.initPath.replace('/','')];
    this.browserService.getFolderContent('/'+this.rootFolder+this.initPath)
      .subscribe(
        (response:Response)=>{
          this.filesList=response.json()['files'];
        }
      )
  }

  getPath(path:string){
    this.browserService.getFolderContent("/"+path)
      .subscribe(
        (response:Response)=>{
          this.filesList=response.json()['files'];
          this.topFolder=this.browserService.getBreadcrumbs(response.json()['topFolder']);
        }
      )
  }

  getPathFromList(directory:String){
    let path:string=this.rootFolder;
    if(this.topFolder!=null) {
      for (let file of this.topFolder) {
        path += '/' + file;
      }
    }
    path+='/'+directory;
    this.getPath(path);
  }


  goToBreadcrumb(crumb:string){
    let path:string;
    for(let file of this.topFolder){
      if(path==null){
        path=this.rootFolder+'/'+file;
      }else{
        path+='/'+file;
      }
      if(file===crumb){
        break;
      }
    }
    this.getPath(path);
  }



}
