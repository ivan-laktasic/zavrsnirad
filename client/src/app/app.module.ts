import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserComponent} from './browser/browser.component';
import {BrowserService} from "./services/browser.service";
import {UploaderService} from "./services/uploader.service";
import {HttpModule} from "@angular/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdButtonModule,
  MdCardModule,
  MdDialogModule,
  MdGridListModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdProgressSpinnerModule,
  MdToolbarModule
} from '@angular/material';
import {UploaderComponent} from './uploader/uploader.component';
import {UploaderModalComponent} from './uploader/uploader-modal/uploader-modal.component';
import {MusicComponent} from './music/music.component';
import {PicturesComponent} from './pictures/pictures.component';
import {VideoComponent} from './video/video.component';
import {AppRoutingModule} from "./app-routing.module";
import {MenuComponent} from './menu/menu.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {ListItemComponent} from './browser/list-item/list-item.component';
import {PicturePropertiesDialogComponent} from './browser/picture-properties-dialog/picture-properties-dialog.component';
import {VideoPropertiesDialogComponent} from './browser/video-properties-dialog/video-properties-dialog.component';
import {MusicPropertiesDialogComponent} from './browser/music-properties-dialog/music-properties-dialog.component';
import {FileDataService} from "./services/fileData.service";
import {FormsModule} from "@angular/forms";
import { PictureViewerDialogComponent } from './browser/picture-viewer-dialog/picture-viewer-dialog.component';
import {EmitterService} from "./services/emitter.service";


@NgModule({
  declarations: [
    AppComponent,
    BrowserComponent,
    UploaderComponent,
    UploaderModalComponent,
    MusicComponent,
    PicturesComponent,
    VideoComponent,
    MenuComponent,
    ListItemComponent,
    PicturePropertiesDialogComponent,
    VideoPropertiesDialogComponent,
    MusicPropertiesDialogComponent,
    PictureViewerDialogComponent
  ],
  entryComponents: [
    UploaderModalComponent,
    PicturePropertiesDialogComponent,
    VideoPropertiesDialogComponent,
    MusicPropertiesDialogComponent,
    PictureViewerDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    MdCardModule,
    MdIconModule,
    MdMenuModule,
    MdButtonModule,
    MdListModule,
    MdToolbarModule,
    MdDialogModule,
    AppRoutingModule,
    MdGridListModule,
    FlexLayoutModule,
    MdProgressSpinnerModule,
    MdInputModule,
    FormsModule
  ],
  providers: [
    BrowserService,
    UploaderService,
    FileDataService,
    EmitterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
