import {FileModel} from "./file.model";

export class MusicFile extends FileModel{
  artist:string;
  album:string;
  genre:string;


  constructor(filename: string, location: string, type: any, size: string, date: Date, artist: string, album: string, genre: string) {
    super(filename, location, type, size, date);
    this.artist = artist;
    this.album = album;
    this.genre = genre;
  }
}
