import {FileModel} from "./file.model";

export class PictureFile extends FileModel{
  description:string;


  constructor(filename: string, location: string, type: string, size: string, date: Date, description: string) {
    super(filename, location, type, size, date);
    this.description = description;
  }
}
