import {FileModel} from "./file.model";

export class VideoFile extends FileModel{
  director:string;
  actors:string;
  genre:string;


  constructor(filename: string, location: string, type: string, size: string, date: Date, director: string, actors: string, genre: string) {
    super(filename, location, type, size, date);
    this.director = director;
    this.actors = actors;
    this.genre = genre;
  }
}
