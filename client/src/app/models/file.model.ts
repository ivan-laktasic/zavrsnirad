export class FileModel{
  filename:string;
  location:string;
  type:any;
  size:string;
  date:Date;


  constructor(filename: string, location: string, type: any, size: string, date: Date) {
    this.filename = filename;
    this.location = location;
    this.type = type;
    this.size = size;
    this.date = date;
  }
}
