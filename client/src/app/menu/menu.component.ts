import {Component, OnInit} from '@angular/core';
import {MdGridListModule} from '@angular/material';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  colsNum: any = 3;

  constructor(public grid:MdGridListModule) { }

  ngOnInit() {
  }

  onResize(event) {
    const element = event.target.innerWidth;
    console.log(element);


    if (element < 950) {
      this.colsNum = 1;
    }

    if (element > 950) {
      this.colsNum = 3;
    }

  }

}
