import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MusicComponent} from "./music/music.component";
import {PicturesComponent} from "./pictures/pictures.component";
import {VideoComponent} from "./video/video.component";
import {MenuComponent} from "./menu/menu.component";

const appRoutes: Routes=[
  {path:'',component:MenuComponent},
  {path:'music',component:MusicComponent},
  {path:'pictures',component:PicturesComponent},
  {path:'videos',component:VideoComponent}
]

@NgModule({
  imports:[RouterModule.forRoot(appRoutes)],
  exports:[RouterModule]
})
export class AppRoutingModule{

}
