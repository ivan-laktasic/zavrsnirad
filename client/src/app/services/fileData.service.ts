import {EventEmitter, Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {PictureFile} from "../models/picture.file.model";
import {VideoFile} from "../models/video.file.model";
import {MusicFile} from "../models/music.file.model";


@Injectable()
export class FileDataService {

  readonly pictureDataUrl: string = 'http://localhost:8081/pictures';
  readonly videoDataUrl: string = 'http://localhost:8081/videos';
  readonly musicDataUrl: string = 'http://localhost:8081/music';

  filesUpdated= new EventEmitter<boolean>();

  header=new Headers({'Content-Type':'application/json'});

  constructor(private http: Http) {
  }

  getPictureData(location:string){
    return this.http.get(this.pictureDataUrl+'/detail/'+location);
  }

  registerPictureFile(picture: PictureFile) {
    return this.http.post(this.pictureDataUrl,JSON.stringify(picture),{headers:this.header});
  }

  updatePictureFile(picture:PictureFile){
    return this.http.put(this.pictureDataUrl,JSON.stringify(picture),{headers:this.header});
  }

  deletePictureFile(location:String){
    return this.http.delete(this.pictureDataUrl+'/'+location);
  }

  getVideoData(location:string){
    return this.http.get(this.videoDataUrl+'/detail/'+location);
  }

  registerVideoFile(video: VideoFile) {
    return this.http.post(this.videoDataUrl,JSON.stringify(video),{headers:this.header});
  }

  updateVideoFile(video:VideoFile){
    return this.http.put(this.videoDataUrl,JSON.stringify(video),{headers:this.header});
  }

  deleteVideoFile(location: string) {
    return this.http.delete(this.videoDataUrl+'/'+location);
  }

  getMusicData(location:string){
    return this.http.get(this.musicDataUrl+'/detail/'+location);
  }

  registerMusicFile(music: MusicFile) {
    return this.http.post(this.musicDataUrl,JSON.stringify(music),{headers:this.header});
  }

  updateMusicFile(music:MusicFile){
    return this.http.put(this.musicDataUrl,JSON.stringify(music),{headers:this.header});
  }

  deleteMusicFile(location: string) {
    return this.http.delete(this.musicDataUrl+'/'+location);

  }
}
