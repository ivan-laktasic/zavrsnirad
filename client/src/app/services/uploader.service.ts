import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";

@Injectable()
export class UploaderService{
  readonly uploadUrl:string='http://localhost:8081/upload';

  constructor(private http:Http){}

  uploadFile(file:File,override:boolean){
    let formData:FormData = new FormData();
    if(override) {
      formData.append('override', 'true');
    }else{
      formData.append('override', 'false');
    }
    formData.append('uploadFile', file, file.name);
    var headers;
    headers = new Headers();
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${this.uploadUrl}`, formData, options);
  }


}
