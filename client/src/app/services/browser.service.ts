/**
 * Created by ivan on 7/29/17.
 */

import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";

@Injectable()
export class BrowserService{

  readonly filesUrl:string='http://localhost:8081/files/list';

  constructor(private http:Http){}

  getFolderContent(path: string){
      return this.http.get(this.filesUrl+path);
  }


  getBreadcrumbs(path:string){
    let breadcrumbs:Array<string>;
    breadcrumbs = path.split("/");
    breadcrumbs.splice(0,2);
    return breadcrumbs;
  }
}
