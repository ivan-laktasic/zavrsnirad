
import {EventEmitter, Injectable} from "@angular/core";
import {MusicFile} from "../models/music.file.model";
import {VideoFile} from "../models/video.file.model";

@Injectable()
export class EmitterService{
  musicFileToPlay= new EventEmitter<MusicFile>();
  videoFileToPlay= new EventEmitter<VideoFile>();

}
