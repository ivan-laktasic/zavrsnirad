import { Component } from '@angular/core';
import {MdDialogRef} from "@angular/material";
import {UploaderService} from "../../services/uploader.service";
import {FileDataService} from "../../services/fileData.service";

@Component({
  selector: 'app-uploader-modal',
  templateUrl: './uploader-modal.component.html',
  styleUrls: ['./uploader-modal.component.css']
})
export class UploaderModalComponent {
  uploader:boolean=false;
  overrideFileDialog=true;
  successDialog=true;
  loading=true;
  file:File;

  constructor(public dialogRef: MdDialogRef<UploaderModalComponent>,private uploderService:UploaderService,private fileService:FileDataService) {}

  fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.loading=false;
      this.uploader=true;
      this.file = fileList[0];
      this.uploderService.uploadFile(this.file,false).subscribe(
        data=>{
          var fileType=JSON.parse(data['_body']).type;
          this.successDialog=false;
          this.loading=true;

        },
        err=>{
                if(err.status==409){
                  this.uploader=true;
                  this.loading=true;
                  this.overrideFileDialog=false;
                }
              }
        );
    }
  }

  overrideFile(){
    this.loading=false;
    this.uploader=true;
    this.overrideFileDialog = true;
    this.uploderService.uploadFile(this.file,true).subscribe(
      data=> {
        var fileType = JSON.parse(data['_body']).type;
        this.loading=true;
        this.successDialog = false;
      }
    );
  }

  closeDialog(){
    this.fileService.filesUpdated.emit(true);
    this.dialogRef.close();
  }
}
