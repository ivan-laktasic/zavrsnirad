import { Component, OnInit } from '@angular/core';
import {MdDialog,MdDialogRef} from "@angular/material";
import {UploaderModalComponent} from "./uploader-modal/uploader-modal.component";

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {

  constructor(public dialog: MdDialog) { }

  ngOnInit() {
  }

  openUploaderModal(){
    let dialogRef = this.dialog.open(UploaderModalComponent);

  }

}
