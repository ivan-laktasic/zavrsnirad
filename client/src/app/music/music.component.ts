import { Component, OnInit } from '@angular/core';
import {MusicFile} from "../models/music.file.model";
import {EmitterService} from "../services/emitter.service";
import {FileDataService} from "../services/fileData.service";
import {Response} from "@angular/http";

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {

  music:MusicFile;
  musicLocation:string;

  constructor(private emitterService:EmitterService,private fileDataService:FileDataService) {
    emitterService.musicFileToPlay.subscribe((musicFile:MusicFile)=>{
      fileDataService.getMusicData(musicFile.location).subscribe((response:Response)=>{
        this.music=response.json();
        this.music.location=this.music.location.replace(/ /g,'%20');
        this.musicLocation=this.fileDataService.musicDataUrl+'/file/'+this.music.location;
        console.log(this.musicLocation);
      })
    })
  }

  ngOnInit() {
  }

}
