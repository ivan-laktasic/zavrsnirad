import { Component, OnInit } from '@angular/core';
import {VideoFile} from "../models/video.file.model";
import {EmitterService} from "../services/emitter.service";
import {FileDataService} from "../services/fileData.service";
import {Response} from "@angular/http";

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  video:VideoFile;
  videoLocation:string;

  constructor(private emitterService:EmitterService,private fileDataService:FileDataService) {
    emitterService.videoFileToPlay.subscribe((videoFile:VideoFile)=>{
      fileDataService.getVideoData(videoFile.location).subscribe((response:Response)=>{
        this.video=response.json();
        this.video.location=this.video.location.replace(/ /g,'%20');
        this.videoLocation=this.fileDataService.videoDataUrl+'/file/'+this.video.location;
        console.log(this.videoLocation);
      })
    })
  }

  ngOnInit() {
  }

}
