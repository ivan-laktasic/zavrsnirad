module.exports=function (express, fs, config,pictureData,path,serverDirectory) {
    var apiRouter=express.Router();

    apiRouter.get('/detail/*',(req,res)=>{
        pictureData.findOne({location:req.params[0]},(err,picture)=>{
            if(picture!=null)
                res.json(picture);
            else{
                res.status(206).send('file is not in database');
            }
        });
    });

    apiRouter.get('/file/Server/*',(req,res)=>{
        console.log(path.join(serverDirectory,req.params[0]));
        return res.sendFile(path.join(serverDirectory,req.params[0]));
    });

    apiRouter.post('/',(req,res)=>{
        const stats = fs.statSync(req.body.location)
        var picture=new pictureData({
            filename:req.body.filename,
            type:req.body.type.mime,
            location:req.body.location,
            size:stats.size,
            date:new Date()
        })
        picture.save((err)=>{
            res.json(picture);
        })
    })

    apiRouter.put('/',(req,res)=>{
        if(path.basename(req.body.location)!=req.body.filename){
            var oldPath=req.body.location;
            var newPath=req.body.location.replace(path.basename(req.body.location),req.body.filename)
            if(fs.existsSync(newPath)){
                res.status(309).send("File already exists");
            }else{
                fs.rename(req.body.location,newPath,(err)=>{
                    if (err) return console.log(err);
                    req.body.location=newPath;
                    pictureData.findOneAndUpdate({location:oldPath},req.body,function(){
                        res.status(200).send("success");
                    });
                });
            }
        }
    });

    apiRouter.delete('/*',(req,res)=>{
        pictureData.findOne({location:req.params[0]},(error,picture)=>{
            fs.unlink(picture.location,(err)=>{
                if(err) return console.log(err);
                pictureData.findOne({location:req.params[0]}).remove((error, writeOpResult)=>{
                    if(error) return console.log(err);
                    res.status(200).send("success");
                })
            })
        })
    })

    return apiRouter;
}