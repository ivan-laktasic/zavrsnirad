module.exports=function(express,fs,config,readChunk,fileType,upload,pictureData,videoData,musicData){
    var apiRouter = express.Router();
    var upload=upload.single('uploadFile');

    apiRouter.post('/',function (req,res) {
        upload(req,res,function(err)
        {
           if(err){
                res.status(409);
                res.json({
                    success:false
                })
            }else{
                console.log(req.file);

                if (req.file['mimetype'].includes("image")) {
                   pictureUpload(req,res);
                }

                if (req.file['mimetype'].includes("video")) {
                    videoUpload(req,res);
                }

                if (req.file['mimetype'].includes("audio")) {
                    musicUpload(req,res);
                }
           }
        });
    })

    return apiRouter;

    function pictureUpload(req,res){

        /*saves or overrides picture*/
        fs.renameSync(req.file.path, config.pictureFolder + '/' + req.file['originalname']);

        if (req.body.override=='false') {
            /*saves data for new picture*/
            console.log("file uploaded!");
            var picture = new pictureData({
                filename: req.file['originalname'],
                location: config.pictureFolder+'/'+req.file['originalname'],
                type: req.file['mimetype'],
                size: req.file['size'],
                date: new Date()
            });
            picture.save(function (err) {
                if (err) return console.error(err);
                res.json(
                    {
                        success: true,
                        type: req.file['mimetype']
                    });
            });
        }else{
            /*updates data for overriden picture*/
            pictureData.findOneAndUpdate({filename:req.file['originalname']},
                {
                    size: req.file['size'],
                    date:new Date()
                },function(){
                    res.json(
                        {
                            success: true,
                            type: req.file['mimetype']
                        });
                });

        }
    }

    function videoUpload(req,res){

        fs.renameSync(req.file.path, config.videoFolder + '/' + req.file['originalname']);

        if (req.body.override=='false') {
            /*saves data for new video*/
            console.log("file uploaded!");
            var video = new videoData({
                filename: req.file['originalname'],
                location: config.videoFolder+'/'+req.file['originalname'],
                type: req.file['mimetype'],
                size: req.file['size'],
                date: new Date()
            });
            video.save(function (err) {
                if (err) return console.error(err);
                res.json(
                    {
                        success: true,
                        type: req.file['mimetype']
                    });
            });
        }else{
            /*updates data for overriden picture*/
            videoData.findOneAndUpdate({filename:req.file['originalname']},
                {
                    size: req.file['size'],
                    date:new Date()
                },function(){
                    res.json(
                        {
                            success: true,
                            type: req.file['mimetype']
                        });
                });
        }
    }

    function musicUpload(req,res){

        fs.renameSync(req.file.path, config.musicFolder + '/' + req.file['originalname']);

        if (req.body.override=='false') {
            /*saves data for new video*/
            console.log("file uploaded!");
            var music = new musicData({
                filename: req.file['originalname'],
                location: config.musicFolder+'/'+req.file['originalname'],
                type: req.file['mimetype'],
                size: req.file['size'],
                date: new Date()
            });
            music.save(function (err) {
                if (err) return console.error(err);
                res.json(
                    {
                        success: true,
                        type: req.file['mimetype']
                    });
            });
        }else{
            /*updates data for overriden picture*/
            musicData.findOneAndUpdate({filename:req.file['originalname']},
                {
                    size: req.file['size'],
                    date:new Date()
                },function(){
                    res.json(
                        {
                            success: true,
                            type: req.file['mimetype']
                        });
                });
        }
    }
}

