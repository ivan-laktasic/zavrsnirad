module.exports=function(express,fs,config,readChunk,fileType,isSvg){
    var apiRouter = express.Router();

    apiRouter.get('/list/*',function(req,res){
        var filesArray=getResultForPath(req.params[0]);
        res.json({
            files:filesArray,
            topFolder:req.params[0]
        })
    });



    function getResultForPath(path){
        var filesArray=[];
        var files=fs.readdirSync(path);

        files.forEach(file => {
            var type;
            var stats=fs.lstatSync(path+'/'+file);
            if(stats.isFile()){
                type=fileType(readChunk.sync(path+'/'+file, 0, 262));
                if(type==null){

                    if(isSvg(fs.readFileSync(path+'/'+file))){
                        type={
                            mime:"image/svg+xml",
                            ext:"svg"
                        };
                    }
                }
                filesArray.push({
                    filename:file,
                    type:type,
                    location:path+'/'+file
                });
            }
            if(stats.isDirectory()){
                getResultForPath(path+'/'+file).forEach(fil=>{
                    filesArray.push(fil)
                })
            }
        });

        return filesArray;
    }

    return apiRouter;
}





