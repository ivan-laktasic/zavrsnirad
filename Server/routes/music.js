module.exports=function (express, fs, config,musicData,path,serverDirectory) {
    var apiRouter=express.Router();

    apiRouter.get('/detail/*',(req,res)=>{
        musicData.findOne({location:req.params[0]},(err,music)=>{
            if(music!=null)
                res.json(music);
            else{
                console.log(206);
                res.status(206).send('file is not in database');
            }

        });
    });

    apiRouter.get('/file/Server/*',(req,res)=>{
        return res.sendFile(path.join(serverDirectory,req.params[0]));
    });

    apiRouter.post('/',(req,res)=>{
        const stats = fs.statSync(req.body.location)
        var music=new musicData({
            filename:req.body.filename,
            type:req.body.type.mime,
            location:req.body.location,
            size:stats.size,
            date:new Date()
        })
        music.save((err)=>{
            res.json(music);
        })
    })

    apiRouter.put('/',(req,res)=>{
        if(path.basename(req.body.location)!=req.body.filename){
            var oldPath=req.body.location;
            var newPath=req.body.location.replace(path.basename(req.body.location),req.body.filename)
            if(fs.existsSync(newPath)){
                res.status(309).send("File already exists");
            }else{
                fs.rename(req.body.location,newPath,(err)=>{
                    if (err) return console.log(err);
                    req.body.location=newPath;
                    musicData.findOneAndUpdate({location:oldPath},req.body,function(){
                        res.status(200).send("success");
                    });
                });
            }
        }
    });

    apiRouter.delete('/*',(req,res)=>{
        musicData.findOne({location:req.params[0]},(error,music)=>{
            fs.unlink(music.location,(err)=>{
                if(err) return console.log(err);
                musicData.findOne({location:req.params[0]}).remove((error, writeOpResult)=>{
                    if(error) return console.log(err);
                    res.status(200).send("success");
                })
            })
        })
    })

    return apiRouter;
}