module.exports=function (express, fs, config,videoData,path,serverDirectory) {
    var apiRouter=express.Router();

    apiRouter.get('/detail/*',(req,res)=>{
        videoData.findOne({location:req.params[0]},(err,video)=>{
            if(video!=null)
                res.json(video);
            else{
                console.log(206);
                res.status(206).send('file is not in database');
            }

        });
    });

    apiRouter.get('/file/Server/*',(req,res)=>{
        console.log(path.join(serverDirectory,req.params[0]));
        return res.sendFile(path.join(serverDirectory,req.params[0]));
    });

    apiRouter.post('/',(req,res)=>{
        const stats = fs.statSync(req.body.location)
        var video=new videoData({
            filename:req.body.filename,
            type:req.body.type.mime,
            location:req.body.location,
            size:stats.size,
            date:new Date()
        })
        video.save((err)=>{
            res.json(video);
        })
    })

    apiRouter.put('/',(req,res)=>{
        if(path.basename(req.body.location)!=req.body.filename){
            var oldPath=req.body.location;
            var newPath=req.body.location.replace(path.basename(req.body.location),req.body.filename)
            if(fs.existsSync(newPath)){
                res.status(309).send("File already exists");
            }else{
                fs.rename(req.body.location,newPath,(err)=>{
                    if (err) return console.log(err);
                    req.body.location=newPath;
                    videoData.findOneAndUpdate({location:oldPath},req.body,function(){
                        res.status(200).send("success");
                    });
                });
            }
        }
    });

    apiRouter.delete('/*',(req,res)=>{
        videoData.findOne({location:req.params[0]},(error,video)=>{
            fs.unlink(video.location,(err)=>{
                if(err) return console.log(err);
                videoData.findOne({location:req.params[0]}).remove((error, writeOpResult)=>{
                    if(error) return console.log(err);
                    res.status(200).send("success");
                })
            })
        })
    })

    return apiRouter;
}