var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
const readChunk = require('read-chunk');
const fileType = require('file-type');
var mongoose=require('mongoose');
var multer=require('multer');
var app = express();
var config=require('./config');
const path = require('path');
const isSvg = require('is-svg');


mongoose.connect(config.db);

var uploadMulter=multer({
    dest: config.mediaFolder,
    fileFilter:(req,file,callback)=>{
        if(req.body.override=="false") {
            if (file['mimetype'].includes("image")) {
                if (fs.existsSync(config.pictureFolder + '/' + file['originalname'])) {
                    return callback(new Error('File already exists'));
                }
            }
            if (file['mimetype'].includes("video")) {
                if (fs.existsSync(config.videoFolder + '/' + file['originalname'])) {
                    return callback(new Error('File already exists'));
                }
            }
            if (file['mimetype'].includes("audio")) {
                if (fs.existsSync(config.musicFolder + '/' + file['originalname'])) {
                    return callback(new Error('File already exists'));
                }
            }
        }
        callback(null,true);
        }
    });

    app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \ Authorization');
    next();
});

/*create necessary files*/
if (!fs.existsSync(config.mediaFolder)){
    fs.mkdirSync(config.mediaFolder);
}
if (!fs.existsSync(config.musicFolder)){
    fs.mkdirSync(config.musicFolder);
}
if (!fs.existsSync(config.videoFolder)){
    fs.mkdirSync(config.videoFolder);
}
if (!fs.existsSync(config.pictureFolder)){
    fs.mkdirSync(config.pictureFolder);
}


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var pictureData=require('./schemas/picture')(mongoose);
var videoData=require('./schemas/video')(mongoose);
var musicData=require('./schemas/music')(mongoose);

var files = require('./routes/files')(express, fs, config, readChunk, fileType,isSvg);
app.use('/files', files);

var upload = require('./routes/upload')(express, fs, config, readChunk, fileType, uploadMulter,pictureData,videoData,musicData);
app.use('/upload', upload);

var pictures = require('./routes/pictures')(express, fs, config,pictureData,path,__dirname);
app.use('/pictures', pictures);

var videos = require('./routes/videos')(express, fs, config,videoData,path,__dirname);
app.use('/videos', videos);

var music = require('./routes/music')(express, fs, config,musicData,path,__dirname);
app.use('/music', music);


app.listen(config.port);
console.log('Running on port ' + config.port);


