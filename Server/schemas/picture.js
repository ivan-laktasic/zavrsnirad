module.exports=function(mongoose){
    var Schema=mongoose.Schema;
    var pictureSchema= new Schema({
        filename:String,
        location:String,
        type:String,
        size:String,
        date:Date,
        description:String
    });

    return mongoose.model('Picture',pictureSchema);
}