module.exports=function(mongoose){
    var Schema=mongoose.Schema;
    var videoSchema= new Schema({
        filename:String,
        location:String,
        type:String,
        size:String,
        director:String,
        actors:String,
        date:Date,
        series:Boolean,
    });

    return mongoose.model('Video',videoSchema);
}