module.exports=function(mongoose){
    var Schema=mongoose.Schema;
    var musicSchema= new Schema({
        filename:String,
        location:String,
        type:String,
        size:String,
        date:Date,
        artist:String,
        album:String,
    });

    return mongoose.model('Music',musicSchema);
}