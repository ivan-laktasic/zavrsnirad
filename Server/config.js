module.exports={
    port:  process.env.PORT || 8081,
    mediaFolder:'Server/mediaFolder',
    musicFolder:'Server/mediaFolder/music',
    videoFolder:'Server/mediaFolder/video',
    pictureFolder:'Server/mediaFolder/picture',
    db:'mongodb://localhost:27017/mediaServerDb',
}